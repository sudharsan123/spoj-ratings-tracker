<?php
	$user=$_GET['user'];
	$ac=$_GET['ac'];
	$wa=$_GET['wa'];
	$ce=$_GET['ce'];
	$re=$_GET['re'];
	$tle=$_GET['tle'];
?>
<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Submission', 'Count'],
          ['AC',<?=$ac?>],
          ['WA',<?=$wa?>],
          ['CE',<?=$ce?>],
          ['RE',<?=$re?>],
          ['TLE',<?=$tle?>]
        ]);
        var options = {
          title: '<?=$user?>\'s Submissions',
          backgroundColor:'#F6F9E0'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 900px; height: 500px;style='background:#E8EDD0'"></div>
  </body>
</html>

