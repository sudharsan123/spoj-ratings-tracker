// ==UserScript==
// @name         SPOJ Ratings Tracker
// @namespace    spoj
// @include      https://*.spoj.com/users/*
// @include      http://*.spoj.com/users/*
// @author       Sudharsan Mohan
// @description  This userscript adds user ratings fetched from VNOI into the user's account page
// ==/UserScript==

function loadScript(url, callback)
{
    // adding the script tag to the head as suggested before
   var head = document.getElementsByTagName('head')[0];
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = url;

   // then bind the event to the callback function 
   // there are several events for cross browser compatibility
   script.onreadystatechange = callback;
   script.onload = callback;

   // fire the loading
   head.appendChild(script);
}
function main() {
	
	if(document.body)
	{
		var handle = document.location.pathname.match(/\/users\/([A-Za-z_0-9]+)/)[1]; 
		x=document.querySelectorAll(".lightrow td");
		a=[]
		for(i=1;i<x.length;i++){
			a.push(x[i].innerText)
		}
		contents=document.querySelector('.content');
		chart1=document.createElement("iframe");
		chart1.src='http://localhost/spoj?user='+handle+'&ac='+a[1]+'&wa='+a[2]+'&ce='+a[3]+'&re='+a[4]+'&tle='+a[5];
		chart1.style.width='100%'
		chart1.style.height='525px'
		chart1.style.border='none'
		contents.appendChild(chart1);
		
		chart2=document.createElement("iframe");
		chart2.src='http://localhost/spoj/graph.php?user='+handle;
		chart2.style.width='100%'
		chart2.style.height='1000px'
		chart2.style.border='none'
		contents.appendChild(chart2);
	}
}
window.onload=main();
