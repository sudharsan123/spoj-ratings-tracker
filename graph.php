<?php
$start=microtime();
$user=$_GET['user'];
if(!$user)	die();
$ch=curl_init("http://www.spoj.com/status/$user/signedlist/");
curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
$str=curl_exec($ch);
$tp=explode("\n",$str);
for($i=0;$i<9;$i++)
	array_shift($tp);
for($i=0;$i<13;$i++)
	array_pop($tp);
$problem;
$stats;
foreach($tp as $tmp){
	$t=explode("|",$tmp);
	$t[3]=trim($t[3]);
	$stats[$t[3]]++;
	if(trim($t[4])=="AC"){
		$problem[$t[3]]=strtotime($t[2]);
	}
}
$datapoints;
foreach($problem as $code=>$timestamp){
	$curr=getdate($timestamp);
	$datapoints[$curr['year']][$curr['mon']]++;
}
ksort($datapoints);
$prev=0;
$dp="['Time','Problems','Count']";
foreach($datapoints as $year=>$month){
	for($i=1;$i<=12;$i++){
		if(!$month[$i])
			$month[$i]=0;
		$prev+=$month[$i];
		if($prev==0)	continue;
		$m=date("F", mktime(0, 0, 0, $i, 10));
		$dp.=",['$m, $year',$prev,$month[$i]]";
	}
}
$onetries;$max=-1;$maxtried;
foreach($stats as $prob=>$tries){
	if($tries==1 && $problem[$prob])	$onetries.="$prob, ";
	if($tries>=$max && $problem[$prob]){
		$maxtried=$prob;
		$max=$tries;
	}
}
$contents=<<<EOT
<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([$dp]);

        var options = {
          title: 'Number of Solved Problems',
          backgroundColor: '#F6F9E0'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="otherstats" style="width: 900px;word-wrap:break-word;">
    	<table border='0'>
    		<tr>
    			<th>Maximum Tries for a single problem</th>
    			<td>$maxtried($max)</td>
    		</tr>
    		<tr>
    			<th>One Shot AC</th>
    			<td>$onetries</td>
    		</tr>
    	</table>
    </div>
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
  </body>
</html>
EOT;
echo $contents;
