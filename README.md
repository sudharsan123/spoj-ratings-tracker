SPOJ Ratings Tracker

This user script fetches contents from the spoj signed list and generates a graph based on the user's submissions.

Installation:

Put the php files in your server at this address: http://localhost/spoj/ 
Open the user.js file with your browser. Browsers usually detect this as an app and requests permissions for installation. Install it and view any user's page on SPOJ

The app uses Google Charts API for rendering the graphs.
